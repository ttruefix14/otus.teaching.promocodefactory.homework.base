﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryEmployeeRepository
        : IRepository<Employee>
    {
        protected IList<Employee> Data { get; set; }

        public InMemoryEmployeeRepository(IList<Employee> data)
        {
            Data = data;
        }
        
        public Task<IList<Employee>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<Employee> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> CreateAsync(Employee baseEntity)
        {
            Data.Add(baseEntity);
            return Task.FromResult(baseEntity.Id);
        }

        public Task<bool> UpdateAsync(Guid id, Employee updateEntity)
        {
            var baseEntity = GetByIdAsync(id).Result;
            if (baseEntity == null)
            {
                return Task.FromResult(false);
            }
            int indexOfBaseEntity = Data.IndexOf(baseEntity);

            baseEntity.FirstName = updateEntity.FirstName ?? baseEntity.FirstName;
            baseEntity.LastName = updateEntity.LastName ?? baseEntity.LastName;
            baseEntity.Email = updateEntity.Email ?? baseEntity.Email;
            baseEntity.Roles = updateEntity.Roles ?? baseEntity.Roles;
            baseEntity.AppliedPromocodesCount = updateEntity.AppliedPromocodesCount | baseEntity.AppliedPromocodesCount;
            
            Data[indexOfBaseEntity] = baseEntity;
            return Task.FromResult(true);
        }

        public Task<bool> Delete(Guid id)
        {
            var baseEntity = GetByIdAsync(id).Result;
            if (baseEntity == null)
            {
                return Task.FromResult(false);
            }
            Data.Remove(baseEntity);
            return Task.FromResult(true);
        }
    }
}