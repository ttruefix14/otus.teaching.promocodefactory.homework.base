﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRoleRepository
        : IRepository<Role>
    {
        protected IList<Role> Data { get; set; }

        public InMemoryRoleRepository(IList<Role> data)
        {
            Data = data;
        }
        
        public Task<IList<Role>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<Role> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> CreateAsync(Role baseEntity)
        {
            Data.Add(baseEntity);
            return Task.FromResult(baseEntity.Id);
        }

        public Task<bool> UpdateAsync(Guid id, Role updateEntity)
        {
            var baseEntity = GetByIdAsync(id).Result;
            if (baseEntity == null)
            {
                return Task.FromResult(false);
            }
            int indexOfBaseEntity = Data.IndexOf(baseEntity);

            baseEntity.Name = updateEntity.Name ?? baseEntity.Name;
            baseEntity.Description = updateEntity.Description ?? baseEntity.Description;
            
            Data[indexOfBaseEntity] = baseEntity;
            return Task.FromResult(true);
        }

        public Task<bool> Delete(Guid id)
        {
            var baseEntity = GetByIdAsync(id).Result;
            if (baseEntity == null)
            {
                return Task.FromResult(false);
            }
            Data.Remove(baseEntity);
            return Task.FromResult(true);
        }
    }
}