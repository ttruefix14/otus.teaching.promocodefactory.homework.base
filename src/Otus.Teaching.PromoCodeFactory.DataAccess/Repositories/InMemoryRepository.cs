﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IList<T> data)
        {
            Data = data;
        }
        
        public Task<IList<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> CreateAsync(T baseEntity)
        {
            Data.Add(baseEntity);
            return Task.FromResult(baseEntity.Id);
        }

        public Task<bool> UpdateAsync(Guid id, T updateEntity)
        {
            var baseEntity = GetByIdAsync(id).Result;
            if (baseEntity == null)
            {
                return Task.FromResult(false);
            }
            Data[Data.IndexOf(baseEntity)] = updateEntity;
            return Task.FromResult(true);
        }

        public Task<bool> Delete(Guid id)
        {
            var baseEntity = GetByIdAsync(id).Result;
            if (baseEntity == null)
            {
                return Task.FromResult(false);
            }
            Data.Remove(baseEntity);
            return Task.FromResult(true);
        }
    }
}