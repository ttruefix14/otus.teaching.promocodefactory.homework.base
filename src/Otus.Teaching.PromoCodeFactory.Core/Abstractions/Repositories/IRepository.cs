﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IList<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<Guid> CreateAsync(T baseEntiny);

        Task<bool> UpdateAsync(Guid id, T updateEntity);
        Task<bool> Delete(Guid id);
    }
}