using Microsoft.OpenApi.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddXmlFile(path:"myConfig.xml", optional: true, reloadOnChange: true);

var options = builder.Configuration.Get<Options>();

//builder.Services.AddSingleton(options);
builder.Services.AddOptions<Options>()
    .Bind(builder.Configuration);
    //.ValidateDataAnnotations();

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "1.0",
        Title = "PromoCode Factory API Doc",
        Description = "������ �� �������� ����������"
    });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    options.IncludeXmlComments(xmlPath, includeControllerXmlComments: true);
});

builder.Services.AddSingleton(typeof(IRepository<Role>), (x) =>
    new InMemoryRoleRepository(FakeDataFactory.Roles));
builder.Services.AddSingleton(typeof(IRepository<Employee>), (x) =>
    new InMemoryEmployeeRepository(FakeDataFactory.Employees));
builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseHsts();
}

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.List);
});

app.UseHttpsRedirection();

app.UseRouting();

app.MapControllers();

app.Run();