﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Extensions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;
using Options = Otus.Teaching.PromoCodeFactory.WebHost.Settings.Options;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        private IOptionsSnapshot<Options> _options;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository, IOptionsSnapshot<Options> options)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
            _options = options;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> Get()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> Get(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var roles = await _roleRepository.GetAllAsync();
            employee.Roles = employee.Roles.FindAll(x => roles.Select(r => r.Name).Contains(x));

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = roles.FirstOrDefault(y => y.Name == x).Id,
                    Name = roles.FirstOrDefault(y => y.Name == x).Name,
                    Description = roles.FirstOrDefault(y => y.Name == x).Description,
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        /// <summary>
        /// Добавить данные о сотруднике
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(Employee employee)
        {
            var roles = await _roleRepository.GetAllAsync();
            employee.Roles = employee.Roles.FindAll(x => roles.Select(r => r.Name).Contains(x));
            var id = await _employeeRepository.CreateAsync(employee);
            return await Task.FromResult(Ok(id));
        }
        /// <summary>
        /// Изменить данные о сотруднике по Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateEmployee"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Put(Guid id, Employee updateEmployee)
        {
            var roles = await _roleRepository.GetAllAsync();
            updateEmployee.Roles = updateEmployee.Roles.FindAll(x => roles.Select(r => r.Name).Contains(x));

            var result = await _employeeRepository.UpdateAsync(id, updateEmployee);
            if (!result)
                return NotFound();

            return await Task.FromResult(Ok(result));
        }
        /// <summary>
        /// Удалить запись о сотруднике по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _employeeRepository.Delete(id);
            if (!result)
                return NotFound();

            return await Task.FromResult(Ok(result));
        }
    }
}