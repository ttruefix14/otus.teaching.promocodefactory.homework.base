﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Settings
{
    public class Secrets
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}