﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Settings
{
    public class Options
    {
        [MaxLength(5)]
        public string MyParam { get; set; }

        public Secrets Secrets { get; set; }
    }
}
